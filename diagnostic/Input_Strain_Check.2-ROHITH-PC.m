A = xlsread('T1_7.5_20.xlsx');
k= A(1,1);
Strain = zeros(length(A),1);
R = 10.16; % Radius (mm) of Pulley at Motor End
L = 330; % Original Length (mm) of Reinforced Fiber
Time = zeros(length(A),1);
for i=1:length(A)
  A(i,1) = A(i,1)-k;
  A(i,2) = pi*A(i,2)/180;
  Strain(i,1) = (A(i,2)*R/L) * 100; % Strain
Time(i,1) = A(i,1); % Relative Time
end


plot(Time+(6.86), Strain)