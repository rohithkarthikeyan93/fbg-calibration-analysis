X2 =  [ 44.523 64.3734   84.1400  104.2783  124.7623  144.3820  164.5203  184.7450  194.5981];
Y2 =  [45 45 45 45 45 45 45 45 45];
Y = [ 0 0 0 0 0 0 0 0 0];
X = [50 70 90 110 130 150 170 190 200];
X3 = [39.2126   58.2627   75.8254   95.4624  115.2550  133.7511  154.1487  172.0761  182.4857];
Y3 = [90 90 90 90 90 90 90 90 90];
X4 = [38.0666   56.0415   74.0622   92.1780  112.7485  132.2818  151.2965  171.0026  181.5471];
Y4 = [120 120 120 120 120 120 120 120 120];
hold on

l1 = [X(1) X2(1) X3(1) X4(1)];
l2 = [X(2) X2(2) X3(2) X4(2)];
l3 = [X(3) X2(3) X3(3) X4(3)];
l4 = [X(4) X2(4) X3(4) X4(4)];
l5 = [X(5) X2(5) X3(5) X4(5)];
l6 = [X(6) X2(6) X3(6) X4(6)];
l7 = [X(7) X2(7) X3(7) X4(7)];
l8 = [X(8) X2(8) X3(8) X4(8)];
l9 = [X(9) X2(9) X3(9) X4(9)];

y = [0 45 90 120];

xlabel ('Angle (^\circC)');
ylabel ('Load (g)');



plot (y,l1,'s');
plot (y,l2,'s');
plot (y,l3,'s');
plot (y,l4,'s');
plot (y,l5,'s');
plot (y,l6,'s');
plot (y,l7,'s');
plot (y,l8,'s');
plot (y,l9,'s');

% plot (Y2,X2,'*r');
% plot (Y,X,'*r');
% plot (Y3,X3,'*r');
% plot(Y4,X4,'*r');

plot (fittedmodel1);
plot (fittedmodel2);
plot (fittedmodel3);
plot (fittedmodel4);
plot (fittedmodel5);
plot (fittedmodel6);
plot (fittedmodel7);
plot (fittedmodel8);
plot (fittedmodel9);

