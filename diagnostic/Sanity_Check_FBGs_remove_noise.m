%% Test Description

% Dynamic Oscillation
% Medium: Air
% Oscillation Amplitude: 80 degrees
% Input Frequency: 1.3 Hz
% Duration: 600 s

%% Dynamics test with instrumented needle

% File Name Convention: 'TestType_Date_InputVariable1_InputVariable2..'
close all;
clear all;
format long
% Read Femto-sense data
a = dlmread('DT_80_10.txt');  
% Extract Wavelength Column:
lambda = a(:,6).*10^12;     

% Conditional to determine which is lambda 1 and which is lamba 2
if lambda(1,1) < lambda(2,1)
  lambda1= lambda(1:2:end); % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda2= lambda(1:2:end); % Isolate Peak 1 Wavelengths
lambda1= lambda(2:2:end);     % Isolate Peak 2 Wavelengths
end

% Sweepcount is a proxy for time
Sweep_count          = a(:,2);              % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);


% Reference wavelengths depend on fiber used - important for accuracy. 
% For the first 1000 values (1 second) - fiber is at rest

lambda1_0 = mean(lambda1(1:1000,:));       % Reference wavelength 1
lambda2_0 = mean(lambda2(1:1000,:));       % Reference wavelength 2

% Sensitivity Parameters  

a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;

% Wavelength Change Values

n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

% Isolate Lambdas of importance

for i = 1:n
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
end
delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;

% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak

time1 = sweepcount1./1000;
time2 = sweepcount2./1000;
time_avg = 0.5*(time1+time2);
hold on
T1 = (delta_strain+abs(delta_strain(1)))*10^-4;
T2 = (delta_strain-abs(delta_strain(1)))*10^-4;

X_R1 = time_avg;
Y_R1 = T1;
Y_R2 = T2;

if(delta_strain(1)<0)
    plot(X_R1,Y_R1,'b'+0.008);
else
    plot(X_R1,Y_R2+0.008,'k');
end

xlim([0 600]);
box on
grid on
xlabel('Time (s)');
ylabel('Measured Strain (%)');
%% FFT Analysis - Dominant Peaks

Fs =    500;
fs1 =   500; % Sampling Frequency
Time1 = 600; % Signal Duration
L1 =    Time1*fs1;

% fc'n'- cut off frequencies w/index
% fs'n' - sampling frequency w/index
fc1 = 60;
fc2 = 70;
fc3 = 80;

Y_R2 = Y_R2-mean(Y_R2);
Y_R1 = Y_R1-mean(Y_R1);

figure
hold on
[b1,a1] = butter(6,fc1/(fs1/2));
freqz(b1,a1)
[b2,a2] = butter(6,fc2/(fs1/2));
freqz(b2,a2);
[b3,a3] = butter(6,fc3/(fs1/2));
freqz(b3,a3);
% For region of interest
if(delta_strain(1)<0)
Y_R1_mod = fft(Y_R1);
P2_1 = abs(Y_R1_mod/L1);
P1_1 = P2_1(1:L1/2+1);
P1_1(2:end-1) = 2*P1_1(2:end-1);
f_1 = Fs*(0:(L1/2))/L1;
% figure
plot(f_1,P1_1)
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
dataOut = filter(b1,a1,Y_R1);
dataOut = filter(b2,a2,dataOut);
dataOut = filter(b3,a3,dataOut);
xlim([0 6]);
else
Y_R1_mod = fft(Y_R2);
P2_1 = abs(Y_R1_mod/L1);
P1_1 = P2_1(1:L1/2+1);
P1_1(2:end-1) = 2*P1_1(2:end-1);
f_1 = Fs*(0:(L1/2))/L1;
figure
plot(f_1,P1_1)
xlim([0 6]);
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
dataOut = filter(b1,a1,Y_R1);
dataOut = filter(b2,a2,dataOut);
dataOut = filter(b3,a3,dataOut);
end

