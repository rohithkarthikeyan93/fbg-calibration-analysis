
% Plot data from sample file

A = csvread('02_19_10T1.csv');
Position = A(:,5);
Time  = A(:,6);
Input = A(:,4);
plot(Time, Position)
title('Input Response for PID - Controller');


hold on
plot (Time, Input)

xlabel('Time (s)');
ylabel('Angular Position (\theta)');


xlim([0 200]);