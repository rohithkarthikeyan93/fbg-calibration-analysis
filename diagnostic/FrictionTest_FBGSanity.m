% Make sense of Friction Test Data
% Rohith Karthikeyan - 11/13/2017

clear all
close all

subplot(2,1,1);
hold on;
FBGSanity('FT_T1.txt')
FBGSanity('FT_T2.txt')
FBGSanity('FT_T3.txt')
legend('Trial 1','Trial 2','Trial 3')
subplot(2,1,2);
hold on;
ATISensorSanity('ATI_T1.csv')
ATISensorSanity('ATI_T2.csv')
ATISensorSanity('ATI_T3.csv')
legend('Trial 1','Trial 2','Trial 3')

function FBGSanity(data)
format long
% Read Femto-sense data
a = dlmread(data);   
% Extract Wavelength Column 120
lambda = a(:,6).*10^12;            
if lambda(1,1) < lambda(2,1)
  lambda1= lambda(1:2:end); % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda2= lambda(1:2:end); % Isolate Peak 1 Wavelengths
lambda1= lambda(2:2:end);   % Isolate Peak 2 Wavelengths
end

Sweep_count          = a(:,2);              % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);

%   
% lambda1_0 = 1.534105501784950e+06;       % Reference wavelength 1
% lambda2_0 = 1.534930278066740e+06;       % Reference wavelength 2

% This value to change depending on fiber in use

lambda1_0= 1543572.68687700;
lambda2_0= 1544391.46266000;

% lambda1_0 = mean(lambda1(1:100,:));       % Reference wavelength 1
% lambda2_0 = mean(lambda2(1:100,:));       % Reference wavelength 2


% Sensitivity Parameters  

a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;

% Wavelength Change Values

n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

for i = 1:n
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
end

delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;

% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak

time1 = sweepcount1./1000;
time2 = sweepcount2./1000;

time_avg = 0.5*(time1+time2);

hold on
T1 = (delta_strain+abs(delta_strain(1)))*10^-4;
T2 = (delta_strain-abs(delta_strain(1)))*10^-4;


% T1(9256:13756,1) = T1(9256:13756,1) +0.08;
% T2(9256:13756,1) = T2(9256:13756,1) +0.08;
%  plot(time_avg,delta_strain,'r')
xlim([0,300]);
ylim([-0.2,3]);
xlabel('Time (s)')
ylabel('Load (N)')
title('FBG Response to Applied Load')
if(delta_strain(1)<0)
   plot(time_avg,32.3495*(T1)+0.0778007);
else
 plot(time_avg,32.3495*(T2)+0.0778007);
end
box on
grid on
end
function ATISensorSanity(data)
readData = csvread(data);
for i = 1:length(readData)
    if (i==1)
    time(i) = 0.001;
    else
    time(i) = time(i-1)+0.001;
    end
end
time = time';
plot(time,readData)
xlim([0,300]);
grid on
box on
ylim([0,3]);
xlabel('Time (s)')
ylabel('Load (N)')
title('ATI F/T Sensor Response to Applied Load')
end
