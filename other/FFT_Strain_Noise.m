% Read File for Data

a = dlmread('T1_2.5_V1.txt');
lambda = a(:,6).*10^12;            

if lambda(1,1) < lambda(2,1)
  lambda1= lambda(1:2:end); % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda2= lambda(1:2:end); % Isolate Peak 1 Wavelengths
lambda1= lambda(2:2:end);   % Isolate Peak 2 Wavelengths
end

Sweep_count          = a(:,2);              % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);

lambda1_0 = mean(lambda1(1:100,:));       % Reference wavelength 1
lambda2_0 = mean(lambda2(1:100,:));       % Reference wavelength 2


% Sensitivity Parameters  

a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;

% Wavelength Change Values

n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

for i = 1:n
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
end

delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;


% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak

time1 = sweepcount1./1000;
time2 = sweepcount2./1000;

time_avg = 0.5*(time1+time2);


T1 = (delta_strain+abs(delta_strain(1)))*10;
T2 = (delta_strain-abs(delta_strain(1)))*10;

% 
% X_R1 = time_avg(122501:132501,1);
% Y_R1 = T1(122501:132501,1);
% Y_R2 = T2(122501:132501,1);

X_R1 = time_avg;
Y_R1 = T1;
Y_R2 = T2;

figure

if(delta_strain(1)<0)
  plot(X_R1,Y_R1+2000,'b');
else
 plot(X_R1,Y_R2+2000,'k');
end

% FFT Analysis - Dominant Peaks

Fs = 250;   % Sampling Frequency
Time1 = 20;
L1 = Time1*Fs;


Y_R2 = Y_R2-mean(Y_R2);
%Y_R2 = detrend(Y_R2);

Y_R1 = Y_R1-mean(Y_R1);

%Y_R1 = detrend(Y_R1);

% fc- cut off frequencies
% fs - sampling frequency

fc2 = 50;
fc1 = 30;
fc3 = 100;
fs1 = 500;


figure
hold on
[b1,a1] = butter(6,fc1/(fs1/2));
freqz(b1,a1)
[b2,a2] = butter(6,fc2/(fs1/2));
freqz(b2,a2);
[b3,a3] = butter(6,fc3/(fs1/2));
freqz(b3,a3);

% For region of interest

if(delta_strain(1)<0)

Y_R1_mod = fft(Y_R1);
P2_1 = abs(Y_R1_mod/L1);
P1_1 = P2_1(1:L1/2+1);
P1_1(2:end-1) = 2*P1_1(2:end-1);

f_1 = Fs*(0:(L1/2))/L1;
figure
plot(f_1,P1_1)
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
dataOut = filter(b1,a1,Y_R1);
dataOut = filter(b2,a2,dataOut);
dataOut = filter(b3,a3,dataOut);

else
    
Y_R1_mod = fft(Y_R2);
P2_1 = abs(Y_R1_mod/L1);
P1_1 = P2_1(1:L1/2+1);
P1_1(2:end-1) = 2*P1_1(2:end-1);

f_1 = Fs*(0:(L1/2))/L1;
figure
plot(f_1,P1_1)
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
dataOut = filter(b1,a1,Y_R1);
dataOut = filter(b2,a2,dataOut);
dataOut = filter(b3,a3,dataOut);
end

figure
plot(X_R1,dataOut)


