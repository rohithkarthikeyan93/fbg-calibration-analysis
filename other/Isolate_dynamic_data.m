
% To begin, Import data using uiimport from Dynamic Testing csv file. 
% Variables Stored as T4_25_10 and VarName2
% Output Counts Data Points and Indexes Accordingly

Replace = 1:length(VarName2);
Replace = Replace';
k=1;
Position = zeros(length(VarName2),1);
Time = zeros(length(VarName2),1);
for i=1:length(VarName2)
if(isnan(VarName2(i))== 1)
Time(i) = Replace(k);
Position(i) = Replace(k);
k=k+1;
else 
   Time(i) = T4_25_10(i);
Position(i) = VarName2(i);
end
end

xlswrite('Dynamic Data Organized',[Position Time]);

    