%% Insertion Sort Data
[Input] = xlsread('S_2.5.xlsx');
k=0.1256^-1;
%V1
xdata1 = Input(2:end,1);
ydata1 = Input(2:end,2);
fdata1 = k*ydata1;

%V3

ydata3 = Input(3:end,8);
xdata3 = Input(3:end,7);
fdata3 = k*ydata3;


%V2

ydata2 = Input(2:end,5);
xdata2 = Input(2:end,4);
fdata2 = k*ydata2;


%V4

ydata4 = Input(2:end,11);
xdata4 = Input(2:end,10);
fdata4 = k*ydata4;


%V5

ydata5 = Input(2:end,14);
xdata5 = Input(2:end,13);
fdata5 = k*ydata5;

%V6

ydata6 = Input(2:end,17);
xdata6 = Input(2:end,16);
fdata6 = k*ydata6;


figure

%% plots
hold on

ylim([-0.0005 0.07])

plot(xdata1,ydata1+0.01692,'r')
plot(xdata2,ydata2+0.0211,'k')
plot(xdata3,ydata3+0.02589,'b')
plot(xdata4,ydata4,'g')
plot(xdata5,ydata5,'y')
plot(xdata6,ydata6-0.01739,'m')



grid on
box on
