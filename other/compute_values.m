function [epsilon_kev_mod] = compute_values(E_of,E_ad,E_kev,L)
x =[0.298 0.292 0.299 0.3086 0.2966 0.359 0.298 0.239 0.178 0.308 0.297 0.307 0.306];


F_of = x./0.1256;
d_of = 0.195 ;
d_kev= 0.083;
d_ad = 0.09;

A_of = pi*(d_of.^2)/4;
A_ad = pi*(d_ad.^2)/4;
A_kev = pi*(d_kev.^2)/4;

A_c = A_of+A_ad;

V_of = A_of./A_c;
ratio = E_of./E_ad;

F_ad = (ratio.*V_of)./((ratio.*V_of)+(1-V_of)).*F_of;

sigma_kev = F_ad./A_kev;
epsilon_kev = sigma_kev./(E_kev.*1000);
% epsilon_observed = 1.33./L;
% fprintf('The measured kevlar fiber strain is %d percent \n',epsilon_kev*100);
% 
% fprintf('The observed kevlar fiber strain is %d percent \n',epsilon_observed*100);
ratio_val = x./(epsilon_kev*100);
epsilon_kev_mod =epsilon_kev*100

 
