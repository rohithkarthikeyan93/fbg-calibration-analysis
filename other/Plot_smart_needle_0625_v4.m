%% Resultant Force

clc
clear all
close all

%%
d = 'C:\Users\rohithkarthikeyan\Syncplicity Folders\BioRobotics (Seok Chang Ryu)\Rohith Karthikeyan\Test Data\Smart Needle\SS-18XTW316-Thick\Axial_Strength\A60\Tube';
dataFiles  = dir([d '\*.csv']);

%%
h = gobjects(length(dataFiles),1);
for i=1:length(dataFiles)
figure
hold on
[h(i), maxy(i)] = analyzeThis(dataFiles(i).name);
end
myStd = std(maxy);
myMean = mean(maxy);
hline = refline([0 myMean]);

function [myPlot,maxy] = analyzeThis(fileName)
myfile = fileName;
FileNameChar = char(myfile);

% identify material
mat = FileNameChar(8:9);
switch mat
    case 'SS'
        Material = 'SS';
    case 'NT'
        Material = 'NiTi';
end

% idenitfy specimen type
typ = FileNameChar(5);
switch typ
    case 'T'
        type = 'Tube';
    case 'P'
        type = 'Prototype';
end

% identify hinge type
hingeType = FileNameChar(2:3);
switch hingeType
    case '60'
        hinge = '60%';
    case '50'
        hinge = '50%';
    case '40'
        hinge = '40%';
end

% identify experiment type
exp = FileNameChar(1);
switch exp
    case 'L'
        expt = 'Lateral';
    case 'A'
        expt = 'Axial';
end

% trial number
tno = FileNameChar(6);
% specify joint type:
joint = 'Hinge';
% input data and read file
inData = table2array(readtable(fileName));
[m,n] = size(inData);
%Isolate content by column and convert to numeric data

if (FileNameChar(11) ~= '1')
for i = 1:n
    for j = 1:m
       outData(j,i) = str2num(cell2mat(inData(j,i)));
    end
end
else
outData = inData;
end

Fx = outData(:,1);
Fy = outData(:,2);
Fz = outData(:,3);

Tx = outData(:,4);
Ty = outData(:,5);
Tz = outData(:,6);


for i = 1:length(Fx)
Fres(i) = sqrt(Fx(i)^2+Fy(i)^2+Fz(i)^2);
Tres(i) = sqrt(Tx(i)^2+Ty(i)^2+Tz(i)^2);
end

for i=1:length(Fres)
timeStamps(i) = i*1/100;
end

myPlot = plot(timeStamps, Fres, '*--')
grid on
box on
t = title({[expt ' | ' mat ' | ' type ...
    ' | ' joint ' | '  hinge ' | ' 'Trial ' tno ]});
xlabel('Time (s)')
ylabel('Resultant load (N)')
maxy = max(Fres);
xlim([0 40]);
plot(xlim, [1 1]*maxy, '--r')
end

% plot average for all files