%% Circular Path Tracing
%uses robotics toolbox by PeterCorke
clc
clear all
close all

%% Kinematics
L(1)= Link('revolute', 'd', 0, 'a', 2.786, 'alpha', -pi/2, 'qlim',[-25 25]);
L(2)= Link('revolute', 'd', 0, 'a', 5.278+0.5, 'alpha', 0, 'qlim',[-25 25]);
toolTip = SerialLink(L, 'name', 'two link');
ax=figure;
count = 0;
endEff=[];

%% Get all tip positions - point cloud of workspace
for theta1 = -25*pi/180:0.1:25*pi/180
    for theta2 = -25*pi/180:0.1:25*pi/180
        count = count+1;
        %toolTip.plot([theta1,theta2]);
        toolTipPositions = toolTip.fkine([theta1,theta2]);
        endEff = [endEff toolTipPositions.t];
    end
end
endEff = endEff';
scatter3(endEff(:,1),endEff(:,2),endEff(:,3),'.');


xlabel('x')
ylabel('Y')
zlabel('Z')
%% Inverse kinematics for joint position:
syms q1 q2;
symTrans = toolTip.fkine([q1,q2]);
symRot = [symTrans.n,symTrans.o,symTrans.a];
td = transl([7 2 2]);
positions=toolTip.ikcon(td)*180/pi;
%% Tendon length change (deltaL):
%-refer old code

%%
%get complete range of motion of the end-effector
%Identify appropriate dimension for circle 
%Solve the inverse kinematics problem
%Convert joint positions to deltaL (tendons)
%Calibrate tendon position to actuator position. 