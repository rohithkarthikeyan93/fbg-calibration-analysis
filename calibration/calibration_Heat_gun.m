
close all;
clear all;
format long
a = dlmread('PM_FBG_TESt_4_500g.txt');   % Read Femto-sense data

lambda = a(:,6).*10^12;             % Extract Wavelength Column

if lambda(1,1) < lambda(2,1)
  lambda1= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda2= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda1= lambda(2:2:end);   % Isolate Peak 2 Wavelengths
end


Sweep_count          = a(:,2);              % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);

  
lambda1_0 = mean(lambda1(1:5000,:));       % Reference wavelength 1
lambda2_0 = mean(lambda2(1:5000,:));       % Reference wavelength 2

% Sensitivity Parameters  

a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;

% Wavelength Change Values

n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

for i = 1:n
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
end

delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;

% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak
time1 = sweepcount1./1000;
time2 = sweepcount2./1000;

time_avg = 0.5*(time1+time2);

grid on

hold on

plot(time_avg,delta_t,'r')
xlabel('time (s)')
ylabel('Temperature change (deg C)')


figure

plot(time_avg,delta_strain,'g')
grid on
xlabel('time (s)')

ylabel('Strain')

% 
% hold on

% X = [delta_strain(12501) delta_strain(25001) delta_strain(45001) delta_strain(60001) delta_strain(65001) delta_strain(80001) delta_strain(87501) delta_strain(97501)]
% Y = [0 50 250 350 450 470 490 500]
% plot(Y,X,'*--r')
% grid on
% x1 = xlabel ('Load (g)')
% y1 = ylabel ('\muStrain')
% set(x1,'FontSize',14);
% set(y1,'FontSize',14);
% t= title('Strain and Temperature Behavior during Loading')
% set(t,'FontSize',14);
% 
% yyaxis right
% 
% T = [delta_t(12501) delta_t(25001) delta_t(45001) delta_t(60001) delta_t(65001) delta_t(80001) delta_t(87501) delta_t(97501)]
% T=T+25;
% plot(Y,T,'b')
% y2 = ylabel('Temperature (^\circC)')
% set(y2,'FontSize',14);
% ylim([0 100]);
% 
% leg = legend('Strain Change','Temperature','Location','northwest')
% set(leg,'FontSize',14);
