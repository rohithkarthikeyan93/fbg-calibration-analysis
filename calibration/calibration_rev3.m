
close all;
clear all;
format long
a = dlmread('PM_FBG_TEST_4_500g.txt');   % Read Femto-sense data

lambda = a(:,6).*10^12;             % Extract Wavelength Column
lambda1= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths

Sweep_count     = a(:,2);               % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);

  
lambda1_0 = mean(lambda1(1:10000,:));       % Reference wavelength 1
lambda2_0 = mean(lambda2(1:10000,:));       % Reference wavelength 2

% Sensitivity Parameters  

%%%%% Code changed below %%%%%
a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;
%%%%%

% Wavelength Change Values

n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

for i = 1:n
    %%%%%% Code changed below %%%%%
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
    %%%%%
end

delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;

% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak
time1 = sweepcount1./1000;
time2 = sweepcount2./1000;

time_avg = 0.5*(time1+time2);

plot(time_avg,delta_t)
xlabel('time (s)')
ylabel('Temperature change (deg C)')

figure

plot(time_avg,delta_strain)
xlabel('time (s)')
ylabel('Strain')


              


