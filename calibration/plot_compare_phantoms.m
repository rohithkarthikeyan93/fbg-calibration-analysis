%Phantom-wise strain difference

Position = [0 2.5 5 7.5 10];
Strain_air = [0 923.33 1675.67 2355.33 2955];
Strain_4_1= [0 753 1521 2123.67 2639];
Strain_8_1=[0, 604.87, 1295, 1903,2422];
Strain_eco = [0, 368, 857, 1393, 1976];

hold on
grid on

%Raw Plot
plot(Position, Strain_air,'sk','MarkerFaceColor','r');
plot(Position, Strain_4_1,'sk','MarkerFaceColor','b');
plot(Position, Strain_8_1,'sk','MarkerFaceColor','g');
plot(Position, Strain_eco,'sk','MarkerFaceColor','y');

legend('In-air','4:1 Plastisol', '8:1 Plastisol', 'Eco-Flex Phantom');
%Fit Data
plot(Air, '--r');
plot(four,'--b');
plot(eight,'g--');
plot(eco,'y--');

xlabel('Angular Position (^\circ)');
ylabel('Micro-Strain (\mu)');
title('Sensorized Tendon Strain during Actuation within Different Phantom Compositions');

